from data import indname, freq, flag, group
if group == 1:
	from fchatgroup import *
else:
	from fchat import *
import numpy as np
import matplotlib.pyplot as plt

import jieba
import jieba.posseg as pseg
import jieba.analyse
import time

jieba.load_userdict("usrdic.txt")

result = ""
timezone = []
talking = 0
datelist = []
timelist = []
for p in indname:
	for i in xrange(len(forus[p].date)-1):	
		for j in forus[p].msg[i]:
			result += j
	timezone.append(forus[p].date[0])
	timezone.append(forus[p].date[-1])
	talking = talking + len(forus[p].date) 
	datelist.extend(forus[p].date)
	timelist.extend(forus[p].time)
talking = talking - len(indname)
timezone.sort()

print "********************************************************"
print "Participants:"
for i in amount:
	print i,":\t",idinfo[i]
print "\nName:\t\t"
for i in indname:
	print i, ":\t", idinfo[i]
print "timezone\t", timezone[0],"to", timezone[-1] 
print "Talking times:\t", talking

if flag == 1:
	print "***********************"
	print freq, "Highest:\n"
hfword0 = []
#hfwordsum = 0
normhfword0 = []
for i in jieba.analyse.extract_tags(result,freq,True):
	#hfwordsum = hfwordsum + i[1]*100
	hfword0.append(i[0])
	if flag == 1:
		print i[0], "\t",i[1]
	normhfword0.append(i[1])#*1.0/hfwordsum)
hfword = hfword0[::-1]
normhfword = normhfword0[::-1]
#words = pseg.cut(result)
#words = jieba.cut(result, cut_all = True)
#words = jieba.cut(result, cut_all = False)
# words = jieba.cut(result)
# #words = jieba.cut_for_search(result)

# wordlist = []
# for w in words:
# 	wordlist.append(w)

if flag == 1:
	print "******************************"
	print "Month Frequency:"
datezone = [0]*12

for j in datelist:
	string = find_between(j, '-', '-')
	if string[0] =='0':
		datezone[int(string[1])-1] = datezone[int(string[1])-1]+1
	else:
		datezone[int(string)-1] = datezone[int(string)-1]+1 
res = sum(datezone)
normdatezone = [0]*12
for i in xrange(12):
	normdatezone[i] = datezone[i]*1.0/res
	if flag == 1:
		print i+1,"\t", datezone[i]

if flag == 1:
	print "******************************"
	print "Time Frequency:"
timezone = [0]*24
for i in xrange(24):
	for j in timelist:
		string = str(j[0:j.find(':')])
		if string == str(i):
			timezone[i] = timezone[i]+1
res = sum(timezone)
normtimezone = [0]*24
for i in xrange(24):
	normtimezone[i] = timezone[i]*1.0/res
	if flag == 1:
		print i, "\t",timezone[i], normtimezone[i]
print "********************************************************"

plt.close('all')
t1 = np.arange(1,13,1)
t2 = np.arange(0,24,1)
fig = plt.figure(1)

plt.subplot(212)
plt.plot(t2,normtimezone, 'k')
plt.title('Time Frequency')

plt.subplot(211)
t1 = tuple(t1)
y_pos = np.arange(len(t1))

plt.barh(y_pos, normdatezone, align = 'center',  alpha=0.4)
plt.yticks(y_pos,t1)
plt.title('Month Frequency')

fig = plt.figure(2)
hfword = tuple(hfword)
y_pos = np.arange(len(hfword))

plt.barh(y_pos, normhfword, align = 'center',  alpha=1.0)
plt.yticks(y_pos,hfword)
plt.xlabel('Frequency')
title = 'Frequency of Highest '+str(freq)+' words'
plt.title(title)

plt.show()